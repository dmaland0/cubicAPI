/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules

//Custom Modules
const dataInstance = require("./coreLogic/databases/dataInstance").dataInstance;

/****************************************
*****************************************
Module Logic*/

/*
  exports.name = function() {
   var instance = new dataInstance(databaseConnectionName);
    //Optional: jsonData fields that should be replaced with "PROTECTED" when protectSecretFields() is called.
   instance.secretFields = ["fieldName1", "fieldName2", ...];
   return instance;
  }
*/

exports.example = function() {
  var instance = new dataInstance("examples");
  return instance;
};
