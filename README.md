# CubicAPI

CubicAPI is a web-application programming foundation and execution environment. The project was forked from openCubes https://gitlab.com/openCubes  in order to be simpler and more streamlined. OpenCubes was originally built to support an ecosystem of microservices delivered to multiple customers in a "logic-and-execution-as-a-service" business model. There was a lot of baked-in complexity as a result, complexity unnecessary for any project outside that business model.

CubicAPI is meant to make creating applications as "frictionless" as practicable, by way of being self-contained and automated wherever possible. (For example, CubicAPI runs its own server process, and uses self-contained databases instead of connecting to another database server.)

You can, of course, use this system to serve a simple website without any "application" functionality at all. This software can certainly not lay claim to being a full replacement for Apache, Nginx, IIS, or any other web server, but it can be an entirely workable alternative.

---

---

## Installation

### Basics

1. Ensure that Node.js and NPM are installed on the execution platform of your choice. An LTS release of Node is strongly recommended for the best chance of a quick, successful install and start with CubicAPI. Also, a SQLite database editor, like SQLiteStudio, is a good thing to have handy.

2. Find the place in your filesystem where you want the code directory to reside, and then clone down the repository.

    a. Unless you are developing directly for CubicAPI, you should throw out the .git directory and run `git init` to start a new project. Remember to review .gitignore to make sure it makes sense for you!

3. The git repository does NOT contain all the dependencies needed for the system to run. You will need to execute `npm install` from within the `coreLogic` directory to get the third-party modules in place. Also - and this is VERY important - the `ui` directory has to be populated in order for openCubes to work "out of the box." A default UI is available within `/coreLogic/basicUI`. Just copy the contents over to the `ui` directory. Please note that whatever is contained inside `ui` is gitignored by default.

4. `EnvironmentVariables.js` needs to be present in the project root and contain appropriate values for your application. `EnvironmentVariablesExample.js` is provided as a starting point:

    * The verbose value indicates whether or not you want CubicAPI to dump out the `environmentVariables` module to the console at startup. This information can be very helpful for development and debugging at first, but can also be unwelcome clutter after a short time. Setting this value to false does NOT prevent critical console messages, errors, and warnings from being displayed.

    * The `insecureRequestPort` value is where you expect a regular HTTP request to arrive. These requests receive a 307 redirect to the requested host, but at the secure `serverPort`. If the port is already in use by the machine (real, virtual, docker container, etc.), then you will get an `EACCESS` exception and a crash. Note that if you want to use port 80 (the conventional port for HTTP), you will likely need to have root/ system superuser/ system administrator level access on the executing machine.

    * The `serverPort` value is where the secure server listens. Like `insecureRequestPort`, a value already in use by the execution platform will cause Node to throw an exception and exit. Note that if you want to use port 443 (the conventional port for HTTPS), you will likely need to have root/ system superuser/ system administrator level access on the executing machine.

    * `allowMultiprocessing` is the setting which controls whether a Cube will try to create a separate process for each CPU available to the execution platform. Setting this to `true` is helpful when you actually do need to maximize the execution performance of a Cube, but it can make debugging difficult: Each process gets duplicate execution code, and determining which process will consume a given request is non-trivial. As such, you should probably refrain from enabling this until you have a stable application...and then, be sure that you beta-test carefully!

    * The `httpsKey` value is any valid path to a key file usable for secure requests. For instance, with Let's Encrypt, the path might be something like `/etc/letsencrypt/live/yourdomain/privkey.pem`.

    * Similarly, `httpsCertificate` is any valid path to a certificate.

    * `httpsChain` is an array of valid paths to files that establish a certificate chain. In some cases, a "fullchain" certificate might be pre-generated, meaning that the array need only have one path string.

      * A quick note about files related to https: On some systems with OpenSSL insalled (like certain Ubuntu machines), you can generate a key and certificate by navigating a terminal to the https directory and executing: `openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 365 -out certificate.pem`. Please note that, on Windows, using a CLI other than CMD or PowerShell may result in output that doesn't work properly. There will be some prompts for you to follow. During initial development and testing, you will very likely be running with self-signed certificates. Modern browsers are (justifiably) picky about allowing this. Bypassing the warnings and/ or allowing your testing certificate in any particular browser is beyond the scope of this document

    * The `defaultfile` entry allows you to specify which file in the "ui" folder is served for a GET request to the default route, "/". This is NOT a file path - it is a file name only.

    * `handlesSubdomains` allows you to specify whether CubicAPI can redirect file requests to different UI subdirectories based on the request subdomain. If true, regular file requests from browsers will have their paths transformed to use the "ui" sub-directory matching the domain or subdomain found on the request.

    * `maxFailedLoginsBeforeThrottle` allows the setting of the login throttler's "strictness."

    * `throttledLoginTimeout` is the number of minutes that login throttling remains in effect after the `maxFailedLoginsBeforeThrottle` number of login attempts is reached.

    * `onlyLogEmails`, when set to true, bypasses the actual sending of emails through `mailer.js` and writes the email content directly to the `logEmails` file.

    * `mailgunUser`, `mailgunPassword`, `mailgunAPIKey`, and `mailgunDomain` are exactly what their names imply. Including them in `environmentVariables.js` is a convenience to make generating Mailgun messages easier across different controllers.

    * `mimeTypes` is an array of arrays, each array storing a relevant file extension at index 0, and the corresponding content-type at index 1. If the system doesn't deliver a certain type of content correctly, adding to this list is usually the first place to start in correcting the problem. Remember to append `;base64` to MIME types that should be transferred as binary files, like images and fonts!

    * `allowedExternalOrigins` is an array of strings that specify addresses allowed for cross origin resource sharing. (This is very important if you intend to serve your UI separately from an API.) To work, any specified external origin must be listed in EXACTLY the way it will appear in an origin header sent by a client. If the origin header matches a string in `allowedExternalOrigins`, then appropriate response `Access-Control` headers are sent and the request should complete normally. If an unknown external origin makes a request, no `Access-Control` response headers are sent, preventing the request from completing.

    * `bypassDeviceAuthentication` allows you to shut off the two-factor-authentication login check that determines if a user's browser (or other HTTP client) is authorized on the server. This can be helpful in development and testing scenarios, but should NOT be enabled in production: Enabling the bypass means that anyone able to steal a user's login credentials can then act as that user immediately. Otherwise, a login from an unrecognized browser or client results in the request being rejected and an alert/ authorization email being sent to the user in question.

5. Once everything is ready, you can run `node server.js` from the `coreLogic` directory to start the server. The command `node --inspect server.js` allows you to attach a debugger during development.

    a. A very helpful alternate is `nodemon`, which can be installed globally via NPM. Nodemon watches for changes to JS files, and automatically restarts the server when those changes occur. Nodemon can be combined with `--inspect` for very effective debugging support during development.

    b. Another inclusion is `coreLogic/runForever.sh`, a Linux shell-script which is useful for running CubicAPI in production (or simulating production). Assuming that Forever has been installed, runForever.sh will attempt to start server.js, automatically restarting when a crash or update happens. The .foreverignore file is used to exclude certain files from Forever's watch list. *RunForever.sh can not be guaranteed to work on your execution platform, and any usage of it should involve careful testing before a public release.*

### The "coreLogic" Directory (And Upgrades)

`CoreLogic` is the directory housing the code that forms the heart of CubicAPI. Keeping the code here makes upgrades easy: Just download the latest changes from the repository, and then replace the old `coreLogic` directory with the new one. Of course, this means that you should NOT edit the contents of `coreLogic` unless you're willing to deal with losing your changes due to an upgrade. Please do remember to make a backup, whatever the case!

### The "ui" Directory

The folder called "ui" is so named because it is meant to house the files used to create a user interface. It's the equivalent of "public" on other web server applications. As such, you should carefully refrain from putting ANYTHING in ui which you don't want everybody able to connect to the server to be able to read. Some files can be restricted to logged-in users, but you still need to be careful.

If no HTML files are found in the ui directory, then CubicAPI will attempt to serve file requests from `coreLogic/basicUI`. However, if ANY files with the .html extension are found in ui, no attempt will be made to serve the basic UI. As a general recommendation, you should copy the contents of `coreLogic/basicUI` to the ui directory and work from there.

### Subdomains

As was presented in the discussion of `environmentVariables.js`, CubicAPI can handle subdomains. This is currently done through the presentation of different UI elements. When subdomains are allowed, the server expects that the `ui` directory has internal directories with names matching the subdomain to act as the root of each subdomain's UI.

The primary domain is itself treated as a subdomain. For instance, "domain.com" is interpreted as being the subdomain "domain," so a `domain` subdirectory would need to be present in the `ui` directory.

### API-Style File Paths

Requests for files that lack a recognized extension from environmentVariables.mimeTypes will be searched for as though they were HTML files. This allows a browser to request an HTML file as something like /example. Internally, the server turns the path into /example.html, and tries to send back the file.

This can, of course, cause unexpected behavior if you're trying to use files without extensions, or files with extensions not registered in environmentVariables.mimeTypes.

### "ManagedUIFile" Functionality

"ManagedUIFile" refers to the server's ability to serve text files from a database. For instance, HTML templates could be stored in the table, ready to be easily edited by users with access to the system.

When given a file request, CubicAPI will always attempt to serve the file from the regular filesystem. If the file read fails, the server will then attempt to find an entry in the main database with a matching name to the original request. If file content is found, it is retrieved and processed like any other file.

You should consider ALL managed UI file entries to be public, so take care not to put any secrets in them!

### Logging

CubicAPI tracks request and response activity in a log database that resides in at `coreLogic/databases/logs.sqlite`. This database can potentially aid in debugging, addressing security issues, and in usage analytics.

The logging system uses a form of blockchain to make the manual modification or deletion of entries more difficult. Each entry has a chain key which is a hash digest of a previous entry's data, including that entry's chain key. If an entry is modified or deleted, then any entry whose chain key depended on the old entry will no longer validate. If the chain keys for the modified entry's descendents are modified, then their descendents will no longer validate, and so on.

It is entirely possible (and not actually very difficult in absolute terms) for someone to write a program which would allow them to update all the necessary chain keys to present a faked entry as being genuine. However, doing so is significantly difficult when compared to simply modifying the database. Thus, the blockchain approach provides some measure of deterrence against simple modifications for nefarious purposes.

### More About Forever

As mentioned previously, Forever is a helper program that is very handy for running CubicAPI in production. It automatically restarts the server process when files are changed, or in the event of a crash. Support for Forever is quite minimalistic, because Forever is technically in the realm of server management. However, a basic Linux shell script and .foreverignore file are available to help you get things running if you do choose to install Forver on a machine.

The .foreverignore file is meant to help with some common problems that Forever encounters with CubicAPI. Rapid-fire server restarts (and failures to start at all) upon database changes and log file modifications are especially vexing, so it's quite important that those be filtered out from Forever's "overwatch."

---

---

## Development, Part 1

#### A Special Note: The Default Superuser

The first user to register on a CubicAPI system is always given superuser privileges. This aids in development and deployment, but it's also a minor security risk that you should be aware of.

### The Basic Rules Of CubicAPI

CubicAPI, like many other application frameworks and foundations, has a certain level of opinionation. The hope is that this opinionation isn't too onerous, while still enforcing a certain "focus."

Out of the box and unmodified, a Cube...

- Only recognizes GET, POST, PUT, DELETE, and OPTIONS verbs.

- Requires valid JSON for POST and PUT verbs as the only acceptable payload (even for file uploads). No other format will be consumed.

- Assumes a design pattern such that an API route is named after the object to be retrieved, created, or modified, and that a parameter can be supplied for identifying a resource. For instance:
    + GET `/api/users/` Retrieves a list of users.
    + GET `/api/users/1` Retrieves the user with the ID "1."
    + POST `/api/users/` Creates a new user.
    + PUT `/api/users/1` Modified the user with the ID "1."
    + DELETE `/api/users/1` Removes the user with the ID "1."

- Generally reserves POST and DELETE operations for users with superuser permissions.

- Operates such that the `requiredFields` array does not prevent a request from having more than is required, but rather defines the "minimum" fields necessary for a request to be considered valid.

#### File Uploads

Because CubicAPI only recognizes JSON as being valid for incoming data, file uploads may be a bit different than some developers are used to. The key to success is supplying the file contents as a base64 encoded string. A minimal but functional example of upload functionality can be found via `coreLogic/basicUI/uploads.html` and the scripts it utilizes.

### RouteDefinitions.js

In the project root, you will find `routeDefinitions.js`, which is where you specify what a request to a certain URL does. Routes can respond to GET, POST, PUT, and DELETE verbs. A separate array of routes is available for each possible verb.

Route definitions rely on controllers to work, so each controller necessary should be required in the `Dependencies` section. For example:

`const someController = require("./controllers/someController");`

Or, to utilize the `usersController` from coreLogic:

`const usersController = require("./coreLogic/controllers/usersController");`

Each of the route arrays mentioned is an array of objects. A route object might look like this:

```Javascript
{
    route: "things/?",
    beforeAction: [anotherController.function],
    action: thingsController.function
    requiredFields: ["aField", "anotherField", ...]
}
```

A "?" character in the `route` field is a parameter that will match anything at that position in the request URL. Other strings must match exactly - so, in the example case, `things/8` and `things/ridiculousParameter` should work, but `thangs/0` (a misspelling) will fail.

The `beforeAction` array is a collection of functions that should be executed in order before `action`. If any `beforeAction` reports failure, the chain of action executions ends, and the server responds with an error.

The `action` is the core controller and function that should be called when the route is matched.

The array of `requiredFields` is helpful for routes that accept data. If all the fields aren't present in the request, the router will reject the request without calling the controller, and send back a message to help with figuring out what was missing. Note that there is no check as to whether the fields contain empty or "falsey" values; The existence of the field is all that's necessary.

#### An Important Note On Actions

With CubicAPI, all actions take the same form. Any action is just a function exported from a Node module that's available to `routeDefinitions.js`. Actions MUST be of this form to work correctly:

```Javascript
function (callObject, resultCallback) {
    //Statements to execute, which can use data from callObject...

    //Then in an appropriate place, perhaps within another callback:
    resultCallback(success, code, [newCookie, anotherNewCookie, etc.], message, data);
}
```

The idea here is that any action can (theoretically) chain into any other action. The `resultCallback` is not actually an HTTP response, but an abstraction of one. This means that controller functionality is more reusable across different controllers: A real response to a request isn't sent until the `resultCallback` originally passed by `router.js` is actually invoked...and `router.js` calls back to `server.js`. Thus, any number of call/ callback cycles can be run by other modules until the `resultCallback` "owned" by `router.js` ends up triggering an HTTP response.

`CallObject` is data for the purpose of future-proofing: If `router.js` adds something to `callObject` for new actions, existing actions simply ignore the new information without having to be modified.

### /api/documentation/

This hardcoded route available sends a cleaned-up `routeDefinitions.js` back to the requester as a JSON object. This is a way for CubicAPI to self-document and make development easier. Do remember, though, that this means your `routeDefinitions.js` file should be considered public. As such, don't put anything in it that should remain secret!

### apiDocumentation.html

This file amounts to a "user-friendly" version of `/api/documentation`, rendered out in an easy to read way with auto-generated forms so that API interactions can be tested. (For it to work properly, you will need to have copied the contents of `/coreLogic/basicUI` into the `ui` directory.)

In certain cases, the default `Include()` paths may need to be modified to accommodate a different directory structure. If apiDocumentation.html doesn't seem to work, looking for and correcting any errors related to the `Include()` tags is an advisable starting place.

### JITRender Tags

"JIT" stands for "Just In Time." When one of these tags is encountered, for example, `<JITRenderServerTime>` a replacement process occurs:

1. The replacement information is stored or generated in a Node module (`justInTimeRenderingActions.js`), by way of function calls that are made to correspond to the second part of the `<JITRender...>` tag and...

2. The replacement information is passed as the single parameter to a callback.

3. The timing of the replacement is just before the server responds with the file; The value is not pre-rendered.

An important thing to remember about using function output is that `fileController.js` calls the functions in a way that's a sort of "naive, brute-force." No matter what the function is, it's fed a single parameter, which is currently assumed to be an object with a field `headers`. The headers in question are the HTTP request headers supplied by the client.

### Include() Tags

Include() tags allow for files to be easily imported into other files. The parentheses are where the file path is specified, for instance `<Include(snippet.html)>`. File includes are always relative to the `ui/` directory...unless the server handles subdomains, which means that the includes are relative to the `ui/subdomain` directory.

Included files can themselves include other files.

### RequiresLogin Tags

RequiresLogin tags prevent Cubes from rendering a text file if the requester is unable to be authenticated via their CubicAPI token.

---

---

## Development, Part 2

### About DataInstance

CubicAPI uses a module-set called "dataInstance" for its internal operations. You are, of course, entirely free to use any database system that Node.js can communicate with. At the same time, dataInstance is very handy for getting started quickly.

  * DataInstance is serverless. It uses Sqlite for everything, which means there's nothing to set up (apart from configuring where your database files are supposed to reside). `DataInstanceDatabaseConnections.js` is the module where database locations are defined.

  * The databases are very simple. Each one is a single table holding a numerical, autoincrementing ID, an altID which can be just about anything that also helps you locate a record directly, and a jsonData field for everything else. Plus, you're never required to set this up yourself, because `sqliteConnectionBuilder.js` creates the data table for you when necessary.

  * The data is malleable. Because you're not dealing with a rigid database structure - outside the minimum necessary, of course - you're free to extend or change the representation of objects as necessary. The database itself is just a managed repository for information that it's largely agnostic about.

  * Operations are abstract. Although the database containers use SQL, you never write any. DataInstance objects use function calls with simple parameters to save, retrieve, and remove database entries.

  * The application programmer is in charge. Because the database itself is very freeform, the application (and the person creating it) is entirely responsible for attaching and enforcing meaningfulness to the data. This does put a lot of weight on the shoulders of the application developer, but it also means that the ways in which data is represented (and interoperates) only has to be defined in one place: The application. Other systems force you to define data representations in the application AND the database.

### Using DataInstance

  * First, find `instanceDefinitions.js` and add a new `exports` entry. The parameter to `new dataInstance()` is very important, because it must exactly match a connection name that you'll define next. Remember to create an `instance.secretFields` array if you want to easily hide sensitive data in responses.

  * Next, open `dataInstanceDatabaseConnections.js` and add a new field in `exports.customConnections`. The field name must match the parameter you supplied for `new dataInstance()` earlier. The database path can be anything that makes sense, although the `../databases` directory used by the `examples` connection is probably a very good choice. (As an aside, yes, you can choose to edit the `coreConnections` object if you need those databases to reside in some other location. Doing so at a whim is inadvisable, however.)

  * Finally, in your controller, require `instanceDefinitions`. You can now create a dataInstance object with something like `var name = new dataInstance.name();`. Now you're ready to interact with the dataInstance API.

### The DataInstance API

A dataInstance object has the following fields (secretFields may or may not be present):

id: null,
altID: null,
data: {},
retrievedFromDatabase: false,
secretFieldsProtected: false,
secretFields: [],
database: databases[databaseName]

The `id`, `altID`, and `data` fields are what tend to be interacted with directly. `AltID` and `data` are the fields most likely to be changed by an application developer, as `id` is meant to be managed internally by dataInstance's own logic.

---

DataInstance objects have the following functions:

#### dataInstance.open = async function(id, altID)

Retrieves the record whose id matches the `id` parameter. If `altID` is supplied, the record is retrieved based on that instead. Returns an error if a database problem occurs, a populated dataInstance if a record is found, or the unmodified dataInstance if no record is retrieved.

Examples:
`var instance = new dataInstance.name();`
`instance = await instance.open(1);`
`instance = await instance.open(null, "email@domain.com");`

---

### dataInstance.openLast = async function()

Retrieves the record with the highest-numbered id. Returns an error if a database problem occurs, a populated dataInstance if a record is found, or the unmodified dataInstance if no record is retrieved.

Examples:
`var instance = new dataInstance.name();`
`instance = await instance.openLast();`

---

### dataInstance.openAll = async function()

Retrieves all records from the database. Returns an error if a database problem occurs, or an object if the retrieval succeeds. The returned object is of the form:

totalInDatabase: number,
instances: {id1: dataInstance, id2: dataInstance, etc.}

Examples:
`var instances = new dataInstance.name();`
`instances = await instances.openAll();`

---

### dataInstance.openMany = async function(startID, limit)

Retrieves multiple records from the database, using the `startID` as the beginning location and continuing until the `limit` number of records have been retrieved (or the database ends). Returns an error if a database problem occurs, or an object if the retrieval succeeds. The returned object is of the form:

totalInDatabase: number,
instances: {id1: dataInstance, id2: dataInstance, etc.}

Examples:
`var instances = new dataInstance.name();`
`instances = await instances.openMany(1, 100);`

---

### dataInstance.find = async function(searchValue)

Retrieves multiple records from the database where the jsonData field contains `searchValue` in any position. Returns an error if a database problem occurs, or an object if the retrieval succeeds. The returned object is of the form:

totalInDatabase: number,
instances: {id1: dataInstance, id2: dataInstance, etc.}

Examples:
`var instances = new dataInstance.name();`
`instances = await instances.find("search");`

---

### dataInstance.save = async function(altID)

Stores a record in the database, setting the altID field to the `altID` parameter. If `altID` is not supplied, the instance's current altID field is used. If the dataInstance has its `retrievedFromDatabase` field set to a value that evaluates as true, then an attempt is made to replace the database entry bearing `dataInstance.id`. In the opposite case, a new record is created. Returns an error message if a database problem occurs, also returns an error if `dataInstance.secretFieldsProtected` is true, or true if the operation is successful.

Examples:
`var instance = new dataInstance.name();`
`instance.data.information = "string";`
`saved = await instance.save("altID");`

`var instance = new dataInstance.name();`
`instance = await instance.open(1);`
`saved = await instance.save("altID");`

---

### dataInstance.delete = async function()

Removes a retrieved record from the database. Returns an error message if a database problem occurs, or true if the operation is successful.


Examples:
`var instance = new dataInstance.name();`
`instance = await instance.open(1);`
`deleted = await instance.delete();`

---

### dataInstance.protectSecretFields = function()

With a retrieved record, sets any fields contained in the `data` field to the value of "PROTECTED", and then sets `dataInstance.secretFieldsProtected` to true. No return value.

Examples:
`var instance = new dataInstance.name();`
`instance = await instance.open(1);`
`instance.protectSecretFields();`
