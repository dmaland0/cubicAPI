/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules

//Custom Modules

/****************************************
*****************************************
Module Logic*/

/****************************************
*****************************************
Custom Database Connections*/

/*Example
{
  databaseName: "./path/relative/to/server.js/name.sqlite"
}
The cubicAPI root directory is ../
The coreLogic directory is ./
*/

exports.customConnections = {
 examples: "../databases/examples.sqlite"
};

/****************************************
*****************************************
Core Database Connections*/

exports.coreConnections = {
  logs: "./databases/logs.sqlite",
  managedUIFiles: "./databases/managedUIFiles.sqlite",
  users: "./databases/users.sqlite",
  registrationKeys: "./databases/registrationKeys.sqlite",
  deviceKeys: "./databases/deviceKeys.sqlite",
  passwordResetKeys: "./databases/passwordResetKeys.sqlite",
  failedLogins: "./databases/failedLogins.sqlite"
};
