/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
//Custom Modules

/****************************************
*****************************************
Module Logic*/

//All function calls should be uniform in their parameters, because fileController doesn't know ahead of time
//what's being called. It's theoretically possible to get around this, but it's probably easier not to.

function exampleJIT (parameterObject, callback) {
  callback("This is the output of exampleJIT, which shows developers a little bit about how JIT rendering works on openCubes.");
}

//JustInTimeRenderingActions are functions defined above.
//Each function must implement a callback which takes the value intended for JIT rendering as a parameter.
exports.justInTimeRenderingActions = {
  Example: exampleJIT
}
