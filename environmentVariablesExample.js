/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
//Custom Modules

/****************************************
*****************************************
Module Logic*/

exports.environmentVariables = {
  verbose: true,
  insecureRequestPort: 8080,
  serverPort: 1024,
  keepLogs: true,
  allowMultiprocessing: false,
  httpsKey: "path/to/https/key",
  httpsCertificate: "path/to/https/certificate",
  httpsChain: ["path/to/https/certificateChain1", "path/to/https/certificateChain2"],
  defaultFile: "index.html",
  handlesSubdomains: false,
  registrationDisabled: true,
  maxFailedLoginsBeforeThrottle: 5,
  throttledLoginTimeout: 5,

  onlyLogEmails: true,
  mailgunUser: "user@domain.tld",
  mailgunPassword: "password",
  mailgunAPIKey: "apiKey",
  mailgunDomain: "domain.tld",

  mimeTypes: [

    ["html", "text/html"],
    ["js", "text/javascript"],
    ["css", "text/css"],
    ["obj", "text/plain"],
    ["png", "image/png;base64"],
    ["jpg", "image/jpg;base64"],
    ["gif", "image/gif;base64"],
    ["svg", "image/svg+xml;base64"],
    ["xml", "application/xml"],
    ["pdf", "application/pdf;base64"],
    ["woff", "application/font-woff;base64"],
    ["woff2", "application/font-woff2;base64"],
    ["ttf", "application/x-font-ttf;base64"],
    ["otf", "application/x-font-otf;base64"],
    ["eot", "application/vnd.ms-fontobject;base64"],
    ["m4v", "video/x-m4v;base64"],
    ["mp4", "video/mp4;base64"]

  ],

  allowedExternalOrigins: [
    "https://server.domain:portIfNecessary"
  ],

  bypassDeviceAuthentication: false

};
