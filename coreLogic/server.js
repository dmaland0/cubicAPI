/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const http = require("http");
const https = require("https");
const fs = require("fs");
const u = require("./utilities/jsUtilities").JSUtilities;
const cluster = require("cluster");
const os = require("os");
//Custom Modules
const logController = require("./controllers/logController");
const router = require("./router");
const environmentVariables = require("../environmentVariables").environmentVariables;

/****************************************
*****************************************
Server Startup Logic*/

//TODO Eventually: Check for key and certificate files, and if they're not found, create them by running
//openssl req -nodes -newkey rsa:2048 -keyout key.pem -out certificate.pem -subj "/C=US/ST=Utah/L=Salt Lake City/O=Global Security/OU=IT Department/CN=example.com"

//Create the processing cluster.

if (cluster.isMaster) {

  console.log("Master process " + process.pid + " started running at " + new Date());
  if (environmentVariables.verbose) {
    console.log("\nReading Environment Variables:\n", environmentVariables);
  }

  if (environmentVariables.allowMultiprocessing) {

    for (let i = 0; i < os.cpus().length; i++) {
      var worker = cluster.fork();
      console.log("\nWorker process " + worker.process.pid + " forked.");
    }

  }

  cluster.on("exit", (worker, code, signal) => {
    console.log("\nWorker process " + worker.process.pid + " died. Signal: " + signal);
  });

}

if ((cluster.isMaster && !environmentVariables.allowMultiprocessing) || (!cluster.isMaster && environmentVariables.allowMultiprocessing)) {

  /*
  respond

  cookies: [] of strings, HTTP cookie
  code: integer, HTTP response code
  contentType: string, HTTP content-type
  body: string or file
  response: A Node HTTP response object

  */
  var respond = function (cookies, code, contentType, body, response) {

    function writeLog () {

      if (environmentVariables.keepLogs) {
        logController.createResponseLogEntry(response);
      }

    }

    //There's no reason to continue if no connection exists. Connections can fail to exist if a response is ended, and then
    //something attempts to use the same response again.
    if (response.connection) {

      if (cookies.length > 0) {
        response.setHeader("Set-Cookie", cookies);
      }

      if (!response.connection.parser) {

        response.connection.parser = {
          incoming: {
            headers: {
              origin: null
            }
          }
        };

      }

      //If the request is from an external origin, it will fail if we don't set certain headers. For security, we WANT the
      //request to fail if the external origin is not known to us in advance, so this block bypasses if the external origin
      //is not defined in environmentVariables.
      if (u.valueIsIn(response.connection.parser.incoming.headers.origin, environmentVariables.allowedExternalOrigins).found) {
        response.setHeader("Access-Control-Allow-Origin", response.connection.parser.incoming.headers.origin);
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
      }

      response.writeHead(code, {"Content-Type": contentType});

      if (contentType.indexOf("base64") >= 0) {
        //Images (amongst other things) need to be encoded properly for the passthrough to work.
        response.end(body, "base64", writeLog);
      } else {
        response.end(body, null, writeLog);
      }

    } else {
      console.warn("Something tried to invoke a response without a connection to send the response to.\nThis isn't fatal for Open Cubes, but your code may be doing something asynchronously that sends a response,\nand then continuing execution until something else tries to send one.");
    }

  };

  //Create a redirecting server for insecure requests.
  http.createServer((request, response) => {
      var host = request.headers.host;

      if (host) {
        host = host.split(":")[0];
      }

      response.writeHead(307, { "Location": "https://" + host + ":" + environmentVariables.serverPort });
      response.end();

  }).listen(environmentVariables.insecureRequestPort);

  //Create the actual server.

  //Load the key and certificate files, plus the certificate chain.

  var httpsOptions = {
    key: fs.readFileSync(environmentVariables.httpsKey),
    cert: fs.readFileSync(environmentVariables.httpsCertificate),
    ca: null
  };

  u.forIn(environmentVariables.httpsChain, function(index, file) {
    var content = fs.readFileSync(file, "utf8");
    httpsOptions.ca += content;
  });

  https.createServer(httpsOptions, function (request, response) {

    if (environmentVariables.keepLogs) {
      logController.createRequestLogEntry(request);
    }

    function consumeRequest() {
      //You have to set the encoding, or you get buffers from requests instead of something readable.
      request.setEncoding("utf-8");
      var allowedVerbs = ["GET","POST","PUT","DELETE", "OPTIONS"];

      if (request.method === "OPTIONS") {
        respond([], 200, "text/plain", "OPTIONS request received.", response);
        return true;
      }

      if (u.valueIsIn(request.method, allowedVerbs).found) {
        //If the request used an allowed verb, continue.

        //Reading POST and PUT payloads requires catching data events and building a data reference to use later.
        var payload = "";

        request.on("data", function(chunk) {
          payload += chunk;
        });

        //When the request ends, try to parse the input and then send relevant data to the routing module.
        request.on("end", function() {

          var failure = false;

          if (request.method === "POST" || request.method === "PUT") {

            try {
              payload = JSON.parse(payload);
            } catch (exception) {
              failure = true;
              var message = "A POST or PUT verb was used, but the supplied data couldn't be parsed as JSON:\n" + exception;
              respond([], 400, "text/plain", message, response);
            }

          }

          if (!failure) {

            //If everything's okay, call the router, and respond when the action is resolved.

            //VERY IMPORTANT: The callback and subsequent call to respond() are bypassed when a file is streamed directly to
            //the response object by fileController.js.
            router.route(request, request.headers, request.url, request.method, payload, response, function(cookies, code, type, body) {
              respond(cookies, code, type, body, response);
            });

          }

      });

      } else {
        //If the request did not use an allowed verb, fail immediately.
        var message = "You used the HTTP verb: " + request.method + ", which is not supported by this server.";
        respond([], 400, "text/plain", message, response);
      }

    }

    consumeRequest();

  }).listen(environmentVariables.serverPort);

}
