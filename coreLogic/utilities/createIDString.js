/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const crypto = require("crypto");
const u = require("../utilities/jsUtilities").JSUtilities;
//Custom Modules

/****************************************
*****************************************
Module Logic*/

var lookup = "0123456789abcdefghijklmnopqrstuvwxyz";

/*
create

Returns a string to be used as an ID.
*/
exports.create = function(callback) {

  crypto.randomBytes(10, function(error, buffer) {

    if (error) {
      callback(false, null);
    } else {

      var idString = "";

      u.forIn(buffer, function(index, value) {
        var lookupLocation = Math.round(((lookup.length - 1)/255)*value);
        idString += lookup[lookupLocation];
      });

      callback(true, idString);

    }

  });

};
