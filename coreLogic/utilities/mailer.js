/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const fs = require("fs");
const u = require("./jsUtilities").JSUtilities;
//Custom Modules
const environmentVariables = require("../../environmentVariables").environmentVariables;
//Dependent Modules
const mailgun = require("mailgun-js")({apiKey: environmentVariables.mailgunAPIKey, domain: environmentVariables.mailgunDomain});

/****************************************
*****************************************
Module Logic*/

/*
sendMail

data {
  from: string, the address that an email should be seen as coming from
  to: string, the destination address for the email
  subject: string, the email subject
  text: string, the email body
}


callback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: string, an error message if necessary

*/
exports.sendMail = function(data, callback) {

  function logEmail(data, returnCall) {

    var logString = "\n==========" + u.makeSQLDatetime();
    var parsedData = u.iterableParser(data);

    u.forIn(parsedData, function(index, string) {
      logString += "\n" + string;
    });

    logString += "\n==========";

    fs.appendFile("../logEmails", logString, function(error) {

      var message = null;

      if (error) {

        message = "\n\nAn error occurred while trying to write to logEmails: " + error;
        returnCall(message);

      } else {

        message = "\n\nEmail data was written to logEmails.";
        returnCall(message);

      }

    });

  }

  if (environmentVariables.onlyLogEmails) {

    logEmail(data, function(message) {

      if (message.indexOf("error") >= 0) {
        callback(false, message);
      } else {
        callback(true, message);
      }

    });

  } else {

    mailgun.messages().send(data, function (error, body) {

        if (error) {
          var parsedError = u.iterableParser(error);
          var errorMessage = "";

          u.forIn(parsedError, function(index, value) {
            errorMessage += value + " ";
          });

          logEmail(data, function(error) {
            errorMessage += error;
            callback(false, errorMessage);
          });

        } else {
          callback(true);
          return body;
        }

    });

  }

};
