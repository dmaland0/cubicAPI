var resetKeyController = new zFrame("resetKeyController");

resetKeyController.validate = function(event) {

	var the = resetKeyController;
	var allValid = true;

	u.forIn(the, function(index, element) {

		if (element && element.tagName === "INPUT") {

			if (element.value.length < 1) {
				allValid = false;
				element.addClass("invalid");
			} else {
				element.removeClass("invalid");
			}

		}

	});

	if (the.Password.value !== the.RepeatPassword.value) {

		allValid = false;
		the.RepeatPassword.addClass("invalid");

	} else {
		the.RepeatPassword.removeClass("invalid");
	}

	if (allValid) {
		the.Submit.disabled = false;
	} else {
		the.Submit.disabled = true;
	}

	return event;

}

resetKeyController.attachUpdateEventsToInputs(resetKeyController.validate);

resetKeyController.Submit.onclick = function(event) {

	event.preventDefault();

	var the = resetKeyController;

	var payload = {};
	payload.key = the.Key.value;
	payload.newPassword = the.Password.value;
	payload.repeatNewPassword = the.RepeatPassword.value;

	payload = JSON.stringify(payload);

	zRequest("PUT", window.location.origin + "/api/users/changePasswordWithKey/", "application/json", [], payload, true, function(xhr) {

		var response = JSON.parse(xhr.response);
		alert(response.message);

		if (response.success) {
			window.location.assign(window.location.origin);
		}

	});

};
