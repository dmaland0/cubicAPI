var loginController = new zFrame("loginController");

loginController.validate = function() {

	var the = loginController;
	var allValid = true;

	if (!u.emailSeemsValid(the.Email.value)) {

		allValid = false;
		the.Email.addClass("invalid");

	} else {
		the.Email.removeClass("invalid");
	}

	if (the.Password.value.length < 1) {

		allValid = false;
		the.Password.addClass("invalid");

	} else {
		the.Password.removeClass("invalid");
	}

	if (allValid) {
		the.Submit.disabled = false;
	} else {
		the.Submit.disabled = true;
	}

}

loginController.Submit.onclick = function(event) {

	event.preventDefault();

	var payload = {};

	payload.email = loginController.Email.value;
	payload.password = loginController.Password.value;
	payload = JSON.stringify(payload);

	zRequest("PUT", window.location.origin + "/api/login/", "application/json", [], payload, true, function(xhr) {
		var response = JSON.parse(xhr.response);
		alert(response.message);

		if (response.success) {
			window.location.assign(location.origin);
		}

	});

};

loginController.attachUpdateEventsToInputs(loginController.validate);
