var resetController = new zFrame("resetController");

resetController.validate = function(event) {

	if (resetController.Email.value !== "") {
		resetController.SubmitEmail.disabled = false;
	} else {
		resetController.SubmitEmail.disabled = "disabled";
	}

	return event;

}

resetController.attachUpdateEventsToInputs(resetController.validate);

resetController.SubmitEmail.onclick = function(event) {

	event.preventDefault();

	var payload = {};

	payload.email = resetController.Email.value;
	payload = JSON.stringify(payload);

	zRequest("PUT", window.location.origin + "/api/passwordResetSendEmail/", "application/json", [], payload, true, function(xhr) {

		var response = JSON.parse(xhr.response);
		alert(response.message);

	});

};
