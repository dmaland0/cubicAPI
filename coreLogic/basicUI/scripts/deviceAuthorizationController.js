var deviceAuthorizationController = new zFrame("deviceAuthorizationController");

deviceAuthorizationController.confirm = function() {

  var code = window.location.search.substr(2);

  var payload = {};
  payload.code = code;

  payload = JSON.stringify(payload);

  zRequest("PUT", window.location.origin + "/api/users/authorizeDevice/", "application/json", [], payload, true, function(xhr) {

    var response = JSON.parse(xhr.response);
    deviceAuthorizationController.Message.innerHTML = response.message;

    if (xhr.status === 200) {

      window.setTimeout(function() {
        window.location.assign(window.location.origin);
      }, 10000);

    }

  });

};

deviceAuthorizationController.confirm();
