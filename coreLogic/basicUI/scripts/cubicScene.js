/*
global
THREE
*/

var axes = ["x", "y", "z"];
var canvas = document.getElementById("threeDCanvas");

var scene = new THREE.Scene();
scene.background = new THREE.Color(document.styleSheets[0].cssRules.item("body").style["background-color"]);
var camera = new THREE.PerspectiveCamera( 50, 400/400, 0.1, 1000 );
camera.position.z = 5.5;

var renderer = new THREE.WebGLRenderer({antialias: true, canvas: canvas});
renderer.setSize(400, 400);
document.body.appendChild(renderer.domElement);

var material = new THREE.MeshPhysicalMaterial( {color: 0x000000, roughness: 0.55, metalness: 0, reflectivity: 1.5, clearCoatRoughness: 0.1, clearCoat: 1} );
var loader = new THREE.OBJLoader();
var cube = null;

var texLoader = new THREE.CubeTextureLoader();
texLoader.setPath("/models/");

var textureCube = texLoader.load( [
	"golden.png", "golden.png",
	"golden.png", "golden.png",
	"golden.png", "golden.png"
] );

material.envMap = textureCube;

loader.load("/models/goldCube.obj",

	function (object) {
		cube = object;

		u.forIn(axes, function(index, axis) {
			cube.scale[axis] = 0.1;
		});

		cube.children[0].material = material;
		scene.add(cube);
	},

	function (xhr) {
		console.log((xhr.loaded/xhr.total * 100 ) + "% loaded");
	},

	function (error) {
		console.log(error);
	}

);

var directionalLight = new THREE.DirectionalLight(0xff8800, 1);
scene.add(directionalLight);

function animate() {
	requestAnimationFrame(animate);
	if (cube) {
		cube.rotation.x += 0.01;
		cube.rotation.y += 0.02;
	}
	renderer.render(scene, camera);
}

animate();
