/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("./utilities/jsUtilities").JSUtilities;
//Custom Modules
const fileController = require("./controllers/fileController");
const routeDefinitions = require("./routeDefinitions");

/****************************************
*****************************************
Module Logic*/

/*
matchRoute

pathElements: array of strings, the parts necessary to construct a path

routeObject: object, a route descriptor

Returns a route object and parameters if the route seems to match the pathElements, or null
*/
function matchRoute(pathElements, routeObject) {
  var routeElements = routeObject.route.split("/");
  var iterator = 0;
  var matchFailure = false;
  var parameters = [];

  //Iterate through route elements by recursion.
  function iterateRouteElements () {

    if (iterator < routeElements.length && matchFailure === false) {

      if (routeElements[iterator] === pathElements[iterator] || (routeElements[iterator] === "?" && pathElements[iterator] !== "")) {
        //Non "?" elements match exactly. A "?" matches anything.

        if (routeElements[iterator] === "?") {
          //A "?" is a parameter.
          parameters.push(pathElements[iterator]);
        }

        //Continue...
        iterator += 1;
        iterateRouteElements();

      } else {

        //A failure to match means there's no need to continue.
        matchFailure = true;

      }

    }

  }

  if (pathElements.length === routeElements.length) {

    //If both the path and route have the same element count, it's worth going forward.
    iterateRouteElements();

    if (!matchFailure) {
      return {"routeObject": routeObject, "parameters": parameters};
    } else {
      return null;
    }

  } else {
    //Differing numbers of elements indicate that the path and route can't be the same.
    return null;
  }

}

/*
route

request: object, the entire incoming request

headers: string, the incoming request headers

path: string, the request address

method: string, the request verb

payload: empty string or object, the request body

serverCallback: function, a callback
  parameter 1: array of strings, the cookies to be passed back to the server
  parameter 2: integer, an HTTP response code to use
  parameter 3: string, a content-type to use for the response
  parameter 4: a response body, which could take many forms

*/
exports.route = function (request, headers, path, method, payload, streamResponse, serverCallback) {

  var allCookies = [];
  var responseObject = {};

  if (headers.cookie) {
    //If there are cookies, split them up so that they can be handled independently.
    allCookies = headers.cookie.split("; ");
  }

  //Allow for token-based authentication.
  if (headers.authorization) {
    var virtualCookies = headers.authorization.substring(7).split("; ");
  }

  if (virtualCookies) {

    u.forIn(virtualCookies, function(index, value) {
      allCookies.push(value);
    });

  }

  //Enable the line below for debugging whether cookies - real or virtual from a Bearer token - are being received.
  //console.log(allCookies, "=====", new Date(), "\n\n");

  //Now, figure out if this is an API request.

  var apiPath = path.split("/api/")[1];

  if (!apiPath) {
    //Handle a non-API (file) request.
    fileController.getFile(path, headers, streamResponse, function(success, message, file) {

      if (!success) {
        serverCallback([], 404, "text/plain", "The requested resource could not be delivered: " + message);
      } else {
        serverCallback([], 200, file.type, file.data);
      }

    });

  } else {
    //Handle an API request.

    var pathElements = apiPath.split("/");
    var matchedRoute = null;
    var type = "application/json";

    if (apiPath === "documentation/") {
      //A special route that helps developers.

      //Iterate through the route definitions, and extract only the readable data.
      var streamlinedRoutes = {
        getRoutes: [],
        postRoutes: [],
        putRoutes: [],
        deleteRoutes: []
      };

      u.forIn(routeDefinitions, function(blockIndex, routeBlock) {

        u.forIn(routeBlock, function(routeIndex, route) {

          var readableRoute = {
            route: route.route,
            description: route.description
          };

          if (route.requiredFields) {
            readableRoute.requiredFields = route.requiredFields;
          }

          streamlinedRoutes[blockIndex].push(readableRoute);

        });

      });

      responseObject = {
          success: true,
          message: "The /api/ route definitions for the Cube were read.",
          data: streamlinedRoutes
        };

      responseObject = JSON.stringify(responseObject);

      serverCallback([], 200, type, responseObject);
      //There's no need to continue executing if this route was called.
      return true;

    }

    //We only search through the routing arrays matching the HTTP verb.
    if (method === "GET") {

      u.forIn(routeDefinitions.getRoutes, function(index, routeObject) {

        if (!matchedRoute) {
          matchedRoute = matchRoute(pathElements, routeObject);
        }

      });

    }

    if (method === "POST") {

      u.forIn(routeDefinitions.postRoutes, function(index, routeObject) {

        if (!matchedRoute) {
          matchedRoute = matchRoute(pathElements, routeObject);
        }

      });

    }

    if (method === "PUT") {

      u.forIn(routeDefinitions.putRoutes, function(index, routeObject) {

        if (!matchedRoute) {
          matchedRoute = matchRoute(pathElements, routeObject);
        }

      });

    }

    if (method === "DELETE") {

      u.forIn(routeDefinitions.deleteRoutes, function(index, routeObject) {

        if (!matchedRoute) {
          matchedRoute = matchRoute(pathElements, routeObject);
        }

      });

    }

    if (matchedRoute) {

      //If we matched a route, see if the required fields are present.
      var requiredFieldMissing = false;

      if (matchedRoute.routeObject.requiredFields) {

        u.forIn(matchedRoute.routeObject.requiredFields, function(index, field) {

          if (!payload.hasOwnProperty(field)) {
            requiredFieldMissing = true;
          }

        });

      }

      if (!requiredFieldMissing) {
        //If all the required fields are present, continue.

        //Build an array of all route actions.
        var allActions = [];

        u.forIn(matchedRoute.routeObject.beforeAction, function(index, action) {
          allActions.push(action);
        });

        allActions.push(matchedRoute.routeObject.action);

        var actionIndex = 0;

        //A recursive function for firing off all of the route's actions, or failing midstream.
        var nextAction = function () {

          var actionFunctionCallObject = {
            parameters: matchedRoute.parameters,
            payload: payload,
            cookies: allCookies,
            requestHeaders: headers,
            request: request
          };

          allActions[actionIndex](actionFunctionCallObject, function(success, code, cookies, message, data) {

            if (success) {
              //If the action succeeds, continue.
              actionIndex += 1;

              if (actionIndex < allActions.length) {
                //If actions remain, continue.
                nextAction();
              } else {
                //If no actions remain, call the server and stop.
                responseObject = {
                  success: success,
                  message: message,
                  data: data
                };

                responseObject = JSON.stringify(responseObject);
                serverCallback(cookies, code, type, responseObject);

              }

            } else {
              //If the action fails, immediately call the server and stop.

              responseObject = {
                success: success,
                message: message,
                data: data
              };

              responseObject = JSON.stringify(responseObject);
              serverCallback(cookies, code, type, responseObject);

            }

          });

        }

        //Initiate the action chain.
        nextAction();

      } else {
        //If necessary fields are missing, fail.
        responseObject = {
          success: false,
          message: "You invoked a route that requires certain input fields, but all the necessary data wasn't supplied.",
          data: {
            yourRequest: payload,
            requiredFields: matchedRoute.routeObject.requiredFields
          }
        };

        responseObject = JSON.stringify(responseObject);
        serverCallback([], 400, type, responseObject);

      }

    } else {

      //If we fail to mach a route, fail with a 404 and a bit of help.
      responseObject = {
        success: false,
        message: "There is no API route that matches your request. Remember that all routes not ending in a parameter should end with a '/'. To see all routes, try the '/api/documentation/' route, or the /apiDocumentation page.",
        data: null
      };

      responseObject = JSON.stringify(responseObject);
      serverCallback([], 404, type, responseObject);

    }

  }

};
