/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../utilities/jsUtilities").JSUtilities;
//Custom Modules
const databases = require("./sqliteConnectionBuilder").databases;

/****************************************
*****************************************
Module Logic*/

var jsonReader = function(data) {

  var returnObject = {success: null, contents: null};

  try {
    var contents = JSON.parse(data);
  } catch (exception) {
    returnObject.success = false;
    returnObject.contents = exception;
    return returnObject;
  }

  returnObject.success = true;
  returnObject.contents = contents;
  return returnObject;

};

exports.dataInstance = function(databaseName) {

  var dataInstance = {
    id: null,
    altID: null,
    data: {},
    retrievedFromDatabase: false,
    secretFieldsProtected: false
  };

  dataInstance.database = databases[databaseName];

  dataInstance.open = async function(id, altID) {
    if (!altID && altID !== 0) {
      var select = dataInstance.database.prepare("SELECT * FROM data WHERE id = ?");
      var identifier = id;
    } else {
      select = dataInstance.database.prepare("SELECT * FROM data WHERE altID = ?");
      identifier = altID;
    }

    const promise = new Promise((resolve, reject) => {

      select.all([identifier], function(error, data) {
        if (error) {
          reject(error);
        } else {

          if (data[0]) {

            dataInstance.id = data[0].id;
            dataInstance.altID = data[0].altID;
            dataInstance.data = jsonReader(data[0].jsonData).contents;
            dataInstance.retrievedFromDatabase = true;
            resolve(dataInstance);

          } else {
            resolve(dataInstance);
          }

        }

      });

    });

    const result = await promise;
    return result;

  };

  dataInstance.openLast = async function() {

    var select = dataInstance.database.prepare("SELECT * FROM data ORDER BY id DESC LIMIT 1");

    const promise = new Promise((resolve, reject) => {

      select.all(function(error, data) {
        if (error) {
          reject(error);
        } else {

          if (data[0]) {

            dataInstance.id = data[0].id;
            dataInstance.altID = data[0].altID;
            dataInstance.data = jsonReader(data[0].jsonData).contents;
            dataInstance.retrievedFromDatabase = true;
            resolve(dataInstance);

          } else {
            resolve(dataInstance);
          }

        }

      });

    });

    const result = await promise;
    return result;

  };

  dataInstance.openAll = async function() {

    var returnObject = {
      totalInDatabase: null,
      instances: {}
    };

    const promise = new Promise((resolve, reject) => {

      var select = dataInstance.database.prepare("SELECT COUNT(*) FROM data;");

      select.all(function(error, data) {

        if (error) {
          reject(error);
        } else {

          returnObject.totalInDatabase = data[0]["COUNT(*)"];
          var select = dataInstance.database.prepare("SELECT * FROM data;");

          select.all([], function(error, data) {

            if (error) {
              reject(error);
            } else {

              u.forIn(data, function(index, row) {
                var instance = new exports.dataInstance(null);
                instance.database = dataInstance.database;
                instance.id = row.id;
                instance.altID = row.altID;
                instance.data = jsonReader(row.jsonData).contents;
                instance.retrievedFromDatabase = true;
                returnObject.instances[row.id] = instance;
              });

              resolve(returnObject);

            }

          });

        }

      });

    });

    const result = await promise;
    return result;

  };

  dataInstance.openMany = async function(startID, limit) {

    var returnObject = {
      totalInDatabase: null,
      instances: {}
    };

    const promise = new Promise((resolve, reject) => {

      var select = dataInstance.database.prepare("SELECT COUNT(*) FROM data;");

      select.all(function(error, data) {

        if (error) {
          reject(error);
        } else {

          returnObject.totalInDatabase = data[0]["COUNT(*)"];
          var select = dataInstance.database.prepare("SELECT * FROM data WHERE id >= ? LIMIT ?;");

          select.all([startID, limit], function(error, data) {

            if (error) {
              reject(error);
            } else {

              u.forIn(data, function(index, row) {
                var instance = new exports.dataInstance(null);
                instance.database = dataInstance.database;
                instance.secretFields = dataInstance.secretFields;
                instance.id = row.id;
                instance.altID = row.altID;
                instance.data = jsonReader(row.jsonData).contents;
                instance.retrievedFromDatabase = true;
                returnObject.instances[row.id] = instance;
              });

              resolve(returnObject);

            }

          });

        }

      });

    });

    const result = await promise;
    return result;

  };

  dataInstance.find = async function(searchValue) {

    var returnObject = {
      totalInDatabase: null,
      instances: {}
    };

    const promise = new Promise((resolve, reject) => {

      var select = dataInstance.database.prepare("SELECT COUNT(*) FROM data;");

      select.all(function(error, data) {

        if (error) {
          reject(error);
        } else {

          returnObject.totalInDatabase = data[0]["COUNT(*)"];
          var query = "SELECT * FROM data WHERE jsonData LIKE '%search%';";
          query = query.replace("search", searchValue);
          var select = dataInstance.database.prepare(query);

          select.all([], function(error, data) {

            if (error) {
              reject(error);
            } else {

              u.forIn(data, function(index, row) {
                var instance = new exports.dataInstance(null);
                instance.database = dataInstance.database;
                instance.secretFields = dataInstance.secretFields;
                instance.id = row.id;
                instance.altID = row.altID;
                instance.data = jsonReader(row.jsonData).contents;
                instance.retrievedFromDatabase = true;
                returnObject.instances[row.id] = instance;
              });

              resolve(returnObject);

            }

          });

        }

      });

    });

    const result = await promise;
    return result;

  };

  dataInstance.save = async function(altID) {

    if (dataInstance.secretFieldsProtected) {
      return "Data instances that have had their secret fields protected can't be saved. You must retrieve an unprotected copy first.";
    }

    if (!altID) {
      altID = dataInstance.altID;
    }

    if (dataInstance.retrievedFromDatabase) {
      var sql = dataInstance.database.prepare("UPDATE data SET altID = ?, jsonData = ? WHERE ID = ?");
      var parameters = [altID, JSON.stringify(dataInstance.data, null, "\t"), dataInstance.id];
    } else {
      sql = dataInstance.database.prepare("INSERT INTO data (altID, jsonData) VALUES (?,?)");
      parameters = [altID, JSON.stringify(dataInstance.data, null, "\t")];
    }

    const promise = new Promise((resolve, reject) => {

      sql.all(parameters, function(error) {
        if (error) {
          reject(error);
        } else {
          resolve(true);
        }

      });

    });

    const result = await promise;
    return result;

  };

  dataInstance.delete = async function() {

    var remove = dataInstance.database.prepare("DELETE FROM data WHERE id = ?;");

    const promise = new Promise((resolve, reject) => {

      remove.all([dataInstance.id], function(error) {

        if (error) {
          reject(error);
        } else {
          resolve(true);
        }

      });

    });

    const result = await promise;
    return result;

  };

  dataInstance.protectSecretFields = function() {

    u.forIn(dataInstance.data, function(key) {
      if (u.valueIsIn(key, dataInstance.secretFields).found) {
        dataInstance.data[key] = "PROTECTED";
      }
    });

    dataInstance.secretFieldsProtected = true;

  };

  return dataInstance;

};
