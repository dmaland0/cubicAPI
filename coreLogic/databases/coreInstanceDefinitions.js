/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules

//Custom Modules
const dataInstance = require("./dataInstance").dataInstance;

/****************************************
*****************************************
Module Logic*/

exports.user = function() {
  var instance = new dataInstance("users");
  instance.secretFields = ["hashedPassword", "accessToken", "salt"];
  return instance;
};

exports.deviceKey = function() {
  var instance = new dataInstance("deviceKeys");
  return instance;
};

exports.failedLogin = function() {
  var instance = new dataInstance("failedLogins");
  return instance;
};

exports.managedUIFile = function() {
  var instance = new dataInstance("managedUIFiles");
  return instance;
};

exports.passwordResetKey = function() {
  var instance = new dataInstance("passwordResetKeys");
  return instance;
};

exports.registrationKey = function() {
  var instance = new dataInstance("registrationKeys");
  return instance;
};

exports.logEntry = function() {
  var instance = new dataInstance("logs");
  return instance;
};
