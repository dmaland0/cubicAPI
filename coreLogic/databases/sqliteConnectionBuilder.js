/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const sqlite3 = require("../node_modules/sqlite3");
const u = require("../utilities/jsUtilities").JSUtilities;
//Custom Modules
const connectionLists = require("../../dataInstanceDatabaseConnections");

/****************************************
*****************************************
Module Logic*/

var coreConnections = connectionLists.coreConnections;
var customConnections = connectionLists.customConnections;

exports.databases = {};

function makeConnections(connectionList) {

  u.forIn(connectionList, function(index, value) {

    var database = new sqlite3.Database(value, function(error) {

      if (error) {
        console.log("There was an error while opening " + value + ": " + error);
      } else {

        var create = database.prepare("CREATE TABLE IF NOT EXISTS data (id INTEGER PRIMARY KEY AUTOINCREMENT, altID STRING, jsonData TEXT)");

        create.all(function(error) {
          if (!error) {
            exports.databases[index] = database;
          } else {
            console.log("There was an error while initializing the database named " + index + ": " + error);
          }

        });

      }

    });

  });

}

makeConnections(coreConnections);
makeConnections(customConnections);
