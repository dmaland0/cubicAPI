/****************************************
*****************************************
Dependencies*/

//Builtin And Third-Party Modules
const u = require("../utilities/jsUtilities").JSUtilities;
const crypto = require("crypto");
//Custom Modules
const dataInstance = require("../databases/coreInstanceDefinitions");

/****************************************
*****************************************
Module Logic*/

/*
generateChainKey

record: object, a complete record from the logging database.
*/
function generateChainKey (record) {

  //Chain keys are hash digests of a linked record's entire entry.
  const hash = crypto.createHash("sha512");
  hash.update(JSON.stringify(record));
  return hash.digest("hex");

}

/*
createRequestLogEntry

request: object, information passed from the server regarding the incoming communication.

*/
exports.createRequestLogEntry = async function(request) {

  var logEntry = new dataInstance.logEntry();
  var log = logEntry.data
  var lastEntry = new dataInstance.logEntry();
  lastEntry = await lastEntry.openLast();

  if (lastEntry.id === null) {
    log.chainKey = 0;
    lastEntry.data = "";
    lastEntry.id = "";
  }

  log.chainKey = generateChainKey(lastEntry.data);
  log.chainPredecessorID = lastEntry.id;
  log.remoteHost = request.connection.remoteAddress;
  log.remotePort = request.connection.remotePort;
  log.headersJSON = JSON.stringify(request.headers);
  log.verb = request.method;
  log.url = request.headers.host + request.url;
  log.statusCode = "N/A";
  log.createdAt = u.makeSQLDatetime();

  var save = await logEntry.save();

  if (save !== true) {
    console.log("WARNING: Logging problem encountered at " + new Date() + ": " + save);
  }

};

/*
createResponseLogEntry

request: object, information passed from the server regarding the outgoing communication.

*/
exports.createResponseLogEntry = async function(response) {

  var logEntry = new dataInstance.logEntry();
  var log = logEntry.data
  var lastEntry = new dataInstance.logEntry();

  lastEntry = await lastEntry.openLast();

  if (lastEntry.id === null) {
    log.chainKey = 0;
    lastEntry.data = "";
    lastEntry.id = "";
  }

  log.chainKey = generateChainKey(lastEntry.data);
  log.chainPredecessorID = lastEntry.id;

  if (response.connection) {
    log.remoteHost = response.connection.remoteAddress;
    log.remotePort = response.connection.remotePort;
  } else {
    log.remoteHost = "Not Available";
    log.remotePort = "Not Available";
  }

  log.headersJSON = response._header;
  log.verb = "N/A";
  log.url = "N/A";
  log.statusCode = response.statusCode;
  log.createdAt = u.makeSQLDatetime();

  var save = await logEntry.save();

  if (save !== true) {
    console.log("WARNING: Logging problem encountered at " + new Date() + ": " + save);
  }

};

/*
getMany

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  request: object, the entire, raw request received by the server
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: array, log entry objects

*/
exports.getMany = async function (callObject, resultCallback) {

  var logEntries = new dataInstance.logEntry();
  logEntries = await logEntries.openMany(callObject.parameters[0], 100);
  var length = u.length(logEntries.instances);

  if (!length) {
    resultCallback(false, 404, [], "No log entries were found given the start index specified.", logEntries);
  } else {
    resultCallback(true, 200, [], "Log entries were found.", logEntries);
  }

};

/*
getOne

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  request: object, the entire, raw request received by the server
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: object, a log entry

*/
exports.getOne = async function(callObject, resultCallback) {

  var logEntry = new dataInstance.logEntry();
  logEntry = await logEntry.open(callObject.parameters[0]);

  if (!logEntry.id) {
    resultCallback(false, 404, [], "No log entries were found with the ID specified: " + callObject.parameters[0], null);
  } else {
    resultCallback(true, 200, [], "A log entry was found.", logEntry);
  }

};

/*
validate

callObject {
  parameters: array of strings, the "?" variables in an API route
  payload: object, data submitted to the server
  cookies: array of strings, the cookies found in the request
  request: object, the entire, raw request received by the server
  requestHeaders: object, the headers from the request
}

resultCallback: function, a callback
  parameter: boolean, whether the operation was successful
  parameter: integer, the relevant HTTP status code
  parameter: array of strings, the cookies to be set
  parameter: string, a message about the operation
  parameter: null

*/
exports.validate = async function (callObject, resultCallback) {

  function compareTwoEntries () {

    var key = generateChainKey(linkedEntry.data);

    if (key !== entryToCheck.data.chainKey) {
      return false;
    } else {
      return true;
    }

  }

  var entryToCheck = new dataInstance.logEntry();
  var linkedEntry = new dataInstance.logEntry();

  entryToCheck = await entryToCheck.open(callObject.parameters[0]);

  if (!entryToCheck.id) {
    resultCallback(false, 404, [], "The specified log entry couldn't be opened.", null);
  } else {
    linkedEntry = await linkedEntry.open(entryToCheck.data.chainPredecessorID);

    if (!linkedEntry.id) {
      resultCallback(false, 404, [], "The specified log entry can't be validated, because its predecessor couldn't be found.", null);
    } else {

      if (compareTwoEntries()) {
        resultCallback(true, 200, [], "Log entry " + entryToCheck.id + " appears valid.", null);
      } else {
        resultCallback(false, 200, [], "Log entry " + entryToCheck.id + " did not validate when checked against " + linkedEntry.id + ".", null);
      }

    }

  }

};
